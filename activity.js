const registeredUsers = [
  "jamieJamie21",
  "45_gunther",
  "KingOfTheWest_2003",
  "freedomFighter47",
  "kennethTheKnight",
  "theScepter07",
  "ninjaismee00",
];

const friendsList = [];

function register(username) {
  const isExistingUser = registeredUsers.includes(username);
  if (isExistingUser) {
    alert("Registration failed. Username already exists!");
    return;
  }
  registeredUsers.push(username);
  alert(`Thank you ${username} for registering!`);
}

function addFriend(username) {
  const foundUser = registeredUsers.find((user) => {
    return user === username;
  });
  if (foundUser) {
    friendsList.push(username);
    alert(`You have added ${username} as a friend!`);
  } else {
    alert("User not found");
  }
}

function displayFriends() {
  const numOfFriends = friendsList.length;
  if (numOfFriends == 0) {
    alert("You currently have 0 friends. Add one first");
  } else {
    console.group("current friends");
    friendsList.forEach((friend) => console.log(friend));
    console.groupEnd();
  }
}

function displayNumberOfFriends() {
  const numOfFriends = friendsList.length;
  if (numOfFriends == 0) {
    alert("You currently have 0 friends. Add one first");
  } else {
    alert(`You currently have ${numOfFriends} friends`);
  }
}

function deleteFriend(user) {
  const numOfFriends = friendsList.length;
  if (numOfFriends == 0) {
    alert("You currently have 0 friends. Add one first");
    return;
  }
  const userIdx = registeredUsers.indexOf(user);
  registeredUsers.splice(userIdx, 1);
  alert(`You deleted ${user} as a friend :(( `);
}
